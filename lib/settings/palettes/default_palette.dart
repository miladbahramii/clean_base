import 'package:flutter/material.dart';

class Palette {}

class DefaultPalette extends Palette {
  final black = Colors.black;
  final white = Colors.white;
}

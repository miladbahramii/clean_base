import 'package:clean_base/core/inventory/palettes/default_palette.dart';

class Inventory {
  static final palette = DefaultPalette();
  static final String fontFamily = 'Fira Code';
  static final String apiRoute = 'example.com';
  static final String userPermissions = 'unimplemented'; //! UNIMPLEMENTED
}

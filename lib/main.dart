import 'package:flutter/material.dart';
import 'core/resourses/app_theme.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Using MultiProvider is convenient when providing multiple objects.
    return MaterialApp(
      // localizationsDelegates: [
      //   GlobalMaterialLocalizations.delegate,
      //   GlobalWidgetsLocalizations.delegate,
      // ],
      supportedLocales: [
        Locale("fa", "IR"),
        Locale("en", "US"),
      ],
      locale: Locale("fa", "IR"),
      title: 'Provider Demo',
      // theme: appTheme,
      home: Container(),
    );
  }
}
